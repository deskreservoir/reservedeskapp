using System;
using System.ComponentModel.DataAnnotations;

namespace DeskReservoir.Api.ApiModels
{
    public class Desk
    {
        [Key]
        public int deskId { get; set; }

        [Required(ErrorMessage = "Desk No is required.")]
        public int deskNo { get; set; }

        [MaxLength(250)]
        public string description { get; set; }

        [Required(ErrorMessage = "Date is required.")]
        public DateTime dateBooked { get; set; }
        
        [Required(ErrorMessage = "FromTime is required.")]
        public string fromTime { get; set; }

        [Required(ErrorMessage = "EndTime is required.")]
        public string toTime { get; set; }

        [Required(ErrorMessage = "Desk Name is required.")]
        [MaxLength(250)]
        public string deskName { get; set; }

        [Required(ErrorMessage = "Availability is required.")]
        public Boolean availability { get; set; }

        [Required(ErrorMessage = "Desk Type is required.")]
        public string deskType { get; set; }
    }
}