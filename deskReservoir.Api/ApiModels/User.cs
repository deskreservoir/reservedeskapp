using System;
using System.ComponentModel.DataAnnotations;

namespace DeskReservoir.Api.ApiModels
{
    public class User
    {
        [Key]
        public int userId { get; set; }
        [Required(ErrorMessage = "Username is required.")]
        [MaxLength(50)]
        public String username { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        [MaxLength(50)]
        public String name { get; set; }
        [Required(ErrorMessage = "Last Name is required.")]
        [MaxLength(50)]
        public String surname { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        public String email { get; set; }
        [Required(ErrorMessage = "Phone Number is required.")]
        public String contact { get; set; }
        [Required(ErrorMessage = "Address is required.")]
        public String address { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [MaxLength(8)]
        public String password { get; set; }
    }

}