using System.Threading.Tasks;
using DeskReservoir.Api.ApiModels;
using DeskReservoir.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace DeskReservoir.Api.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class DesksController : ControllerBase
    {
        protected readonly IDeskService _deskService;
        public DesksController(IDeskService deskService)
        {
            _deskService = deskService;
        }
        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetAllDesks()
        {
            var desks = await _deskService.GetAllDeskAsync();
            return Ok(desks);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetDeskById(int deskId)
        {
            var desk = await _deskService.GetDeskByIdAsync(deskId);
            return Ok(desk);
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> AddDesk([FromBody] Desk desk)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _deskService.AddDesk(desk);
            return CreatedAtAction(nameof(GetDeskById), new { deskId = desk.deskId }, desk);
        }
        [Route("[action]")]
        [HttpPut]
        public async Task<IActionResult> UpdateDesk([FromBody] Desk desk)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _deskService.UpdateDesk(desk);
            return Ok();
        }
        [Route("[action]")]
        [HttpDelete]
        public async Task<IActionResult> DeleteDesk(int Id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _deskService.DeleteDesk(Id);
            return Ok();
        }

    }
}