
using System.Threading.Tasks;
using DeskReservoir.Api.ApiModels;
using DeskReservoir.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace DeskReservoir.Api.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        protected readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        [Route("[action]")]
        [HttpGet]
      
        public async Task<IActionResult> GetAllUsers()
        {
            var users = await _userService.GetAllUsersAsync();
            return Ok(users);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _userService.GetUserByIdAsync(userId);
            return Ok(user);
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Adduser([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _userService.AddUser(user);
            return CreatedAtAction(nameof(GetUserById), new { userId = user.userId }, user);
        }
       // [Route("[action]")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id,[FromBody] User user)
        {
            user.userId = id;
            await _userService.UpdateUser(user);
            return Ok();
        }

        [Route("[action]")]
        [HttpDelete]
        public async Task<IActionResult> DeleteUser(int Id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _userService.DeleteUser(Id);
            return Ok();
        }
    }
}