CREATE TABLE if not exists users(
    userId serial primary key,
    username varchar,
    name varchar,
    surname varchar,
    email varchar,
    contact varchar,
    address varchar,
    password varchar
);