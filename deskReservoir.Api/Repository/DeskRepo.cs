using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using DeskReservoir.Api.ApiModels;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace DeskReservoir.Api.Repository
{
    public class DeskRepo : IDeskRepo
    {
        protected readonly IConfiguration _config;
        private string Connectionstring = "DefaultConnection";
        public DeskRepo(IConfiguration config)
        {
            _config = config;
        }

        //Create Idb connection 
        public IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(_config.GetConnectionString(Connectionstring));
            }
        }

        public async Task<int> AddDesk(Desk desk)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"insert into Desk(deskId,deskNo,description,dateBooked,fromTime,toTime,deskName,availability,deskType) values(@deskId,@deskNo,@description,@dateBooked,@fromTime,@toTime,@deskName,@availability,@deskType)";
                    return await dbConnection.ExecuteAsync(query, desk);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> DeleteDesk(int deskId)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"DELETE FROM Desk WHERE deskid= @deskId";
                    return await dbConnection.ExecuteAsync(query, new { deskId = deskId });
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Desk>> GetAllDeskAsync()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"SELECT deskId,deskNo,description,dateBooked,fromTime,toTime,deskName,availability,deskType FROM Desk";
                    return await dbConnection.QueryAsync<Desk>(query);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Desk> GetDeskByIdAsync(int deskId)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = "SELECT * FROM Desk WHERE deskId = @deskId";
                    return await dbConnection.QueryFirstOrDefaultAsync<Desk>(query, new { deskId = deskId });
                }

            }
            catch (Exception)
            {
                throw ;
            }
        }

        public async Task<int> UpdateDesk(Desk desk)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"UPDATE Desk SET description=@description,dateBooked=@dateBooked,fromTime=@fromTime,toTime=@toTime,deskName=@deskName,availability=@availability,deskType=@deskType WHERE deskId =@deskId";
                    return await dbConnection.ExecuteAsync(query, desk);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}