using System.Collections.Generic;
using System.Threading.Tasks;
using DeskReservoir.Api.ApiModels;

namespace DeskReservoir.Api.Repository
{
    public interface IDeskRepo
    {
        Task<IEnumerable<Desk>> GetAllDeskAsync();
        Task<Desk> GetDeskByIdAsync(int id);
        Task<int> AddDesk(Desk desk);
        Task<int> UpdateDesk(Desk desk);
        Task<int> DeleteDesk(int id);
    }
}