using System.Collections.Generic;
using System.Threading.Tasks;
using DeskReservoir.Api.ApiModels;

namespace DeskReservoir.Api.Repository
{
    public interface IUserRepo
    {
        Task<IEnumerable<User>> GetAllUsersAsync();
        Task<User> GetUserByIdAsync(int id);
        Task<int> AddUser(User user);
        Task<int> UpdateUser(User user);
        Task<int> DeleteUser(int id);
    }
}