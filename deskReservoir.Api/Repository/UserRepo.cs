using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using DeskReservoir.Api.ApiModels;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace DeskReservoir.Api.Repository
{
    public class UserRepo : IUserRepo
    {
        protected readonly IConfiguration _config;
        private string Connectionstring = "DefaultConnection";
        public UserRepo(IConfiguration config)
        {
            _config = config;
        }

        //Create Idb connection 
        public IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(_config.GetConnectionString(Connectionstring));
            }
        }

        public async Task<int> AddUser(User user)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"insert into Users(username,name,surname,email,contact,address,password) values(@username,@name,@surname,@email,@contact,@address,@password)";
                    return await dbConnection.ExecuteAsync(query, user);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> DeleteUser(int userId)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"DELETE FROM Users WHERE userid= @userId";
                    return await dbConnection.ExecuteAsync(query, new { userid = userId });
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"SELECT userId,username,name,surname,email,contact,address,password FROM Users";
                    return await dbConnection.QueryAsync<User>(query);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = "SELECT * FROM Users WHERE userId = @userId";
                    return await dbConnection.QueryFirstOrDefaultAsync<User>(query, new { userId = userId });
                }

            }
            catch (Exception)
            {
                throw ;
            }
        }

        public async Task<int> UpdateUser(User user)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    string query = @"UPDATE Users SET username =@username,name=@name,surname=@surname,email=@email,contact=@contact,address=@address,password=@password WHERE userId =@userId";
                    return await dbConnection.ExecuteAsync(query, user);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}