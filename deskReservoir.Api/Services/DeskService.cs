using System.Collections.Generic;
using System.Threading.Tasks;
using DeskReservoir.Api.ApiModels;
using DeskReservoir.Api.Repository;

namespace DeskReservoir.Api.Services
{
    public class DeskService : IDeskService
    {
        protected readonly IDeskRepo _deskRepository;
        public DeskService(IDeskRepo deskRepository)
        {
            _deskRepository = deskRepository;
        }
        public Task<int> AddDesk(Desk desk)
        {
           return  _deskRepository.AddDesk(desk);
        }

        public Task<int> DeleteDesk(int id)
        {
           return _deskRepository.DeleteDesk(id);
        }

        public Task<IEnumerable<Desk>> GetAllDeskAsync()
        {
            return _deskRepository.GetAllDeskAsync();
        }

        public Task<Desk> GetDeskByIdAsync(int id)
        {
            return _deskRepository.GetDeskByIdAsync(id);
        }

        public Task<int> UpdateDesk(Desk desk)
        {
           return _deskRepository.UpdateDesk(desk);
        }
    }
}