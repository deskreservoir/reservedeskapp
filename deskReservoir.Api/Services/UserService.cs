using System.Collections.Generic;
using System.Threading.Tasks;
using DeskReservoir.Api.ApiModels;
using DeskReservoir.Api.Repository;

namespace DeskReservoir.Api.Services
{
    public class UserService : IUserService
    {
        protected readonly IUserRepo _userRepository;
        public UserService(IUserRepo userRepository)
        {
           _userRepository = userRepository;
        }
        public Task<int> AddUser(User user)
        {
           return  _userRepository.AddUser(user);
        }

        public Task<int> DeleteUser(int id)
        {
           return _userRepository.DeleteUser(id);
        }

        public Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return _userRepository.GetAllUsersAsync();
        }

        public Task<User> GetUserByIdAsync(int id)
        {
            return _userRepository.GetUserByIdAsync(id);
        }

        public Task<int> UpdateUser(User user)
        {
           return _userRepository.UpdateUser(user);
        }
    }
}