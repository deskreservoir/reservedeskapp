using DeskReservoir.Api.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using DeskReservoir.Api.Services;
using Microsoft.EntityFrameworkCore;
using DeskReservoir.Api.Repository;
using System.Reflection;

namespace deskReservoir.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<DataContext.AppContext>(options =>
                          options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            //Register Services    
            services.AddScoped<IDeskService, DeskService>();
            services.AddScoped<IDeskRepo, DeskRepo>();
            services.AddScoped<IUserService,UserService>();
            services.AddScoped<IUserRepo,UserRepo>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "deskReservoir.Api", Version = "v1" });
            });

             services.AddCors(opt =>
            {
                opt.AddPolicy("CorsPolicy", policy =>
                {
                    policy.AllowAnyMethod().AllowAnyHeader().WithOrigins("http://localhost:3000");
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<CustomExceptionHandlingMiddleware>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "deskReservoir.Api v1"));
            }

            app.UseHttpsRedirection();
            app.UseRouting(); 
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            using var scope =app.ApplicationServices.CreateScope();
        }
    }
}
