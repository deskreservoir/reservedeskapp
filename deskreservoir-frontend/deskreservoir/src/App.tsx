import "./App.css";
import LayoutDashboard from "./components/Dashboard/LayoutDashboard";

const App = () => {
  return (
    <div className="">
      <LayoutDashboard />
    </div>
  );
};
export default App;
