import axios, {AxiosResponse} from 'axios';
import {UserType} from "../models/user.interface";
import {DeskType} from "../models/desk.interface";

const instance = axios.create({
    baseURL: 'https://localhost:5001',
    timeout: 15000,
});

const responseBody = (response: AxiosResponse) => response.data;

const requests = {
    get: (url: string) => instance.get(url).then(responseBody),
    post: (url: string, body: {}) => instance.post(url, body).then(responseBody),
    put: (url: string, body: {}) => instance.put(url, body).then(responseBody),
    delete: (url: string) => instance.delete(url).then(responseBody),
};

export const UserApi = {
    getUsers: (): Promise<UserType[]> => requests.get('/api/Users/GetAllUsers'),
    getUser: (id: number): Promise<UserType> => requests.get(`/api/Users/GetUserById?userId=${id}`),
    createUser: (user: UserType): Promise<UserType> =>
        requests.post('/api/Users/Adduser', user),
    updateUser: (user: UserType, id: number): Promise<UserType> =>
        requests.put(`/api/Users/UpdateUser/${id}`, user),
    deleteUser: (id: number): Promise<void> => requests.delete(`/api/Users/DeleteUser?Id=${id}`),
};

export const DeskApi = {
    getDesks: (): Promise<DeskType[]> => requests.get('/api/Desks/GetAllDesks'),
    getDesk: (id: number): Promise<DeskType> => requests.get(`/api/Desks/GetDeskById?deskId=${id}`),
    createDesk: (user: DeskType): Promise<DeskType> =>
        requests.post('/api/Users/Adduser', user),
    updateDesk: (desk: DeskType, id: number): Promise<DeskType> =>
        requests.put(`/api/Desks/UpdateDesk/${id}`, desk),
    deleteUser: (id: number): Promise<void> => requests.delete(`/api/Users/DeleteUser?Id=${id}`),

};