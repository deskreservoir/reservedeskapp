import React from "react";
import {useLocation, Link} from "react-router-dom";
import {Breadcrumb} from "antd";
import {HomeOutlined} from "@ant-design/icons";

import classes from './BreadCrumb.module.css';

const {Item} = Breadcrumb;

const BreadCrumb = () => {
    const location = useLocation();
    const breadCrumbView = () => {
        const {pathname} = location;
        const pathName = pathname.split("/").filter((item) => item);
        const capatilize = (s: any) => s.charAt(0).toUpperCase() + s.slice(1);
        return (
            <div>
                <Breadcrumb className={classes.crumb} >
                    {pathName.length > 0 ? (
                        <Item>
                            <Link to="/">Home</Link>
                        </Item>
                    ) : (
                        <Item> <HomeOutlined/></Item>
                    )}
                    {pathName.map((name, index) => {
                        const routeTo = `/${pathName.slice(0, index + 1).join("/")}`;
                        const isLast = index === pathName.length - 1;
                        return isLast ? (
                            <Item key={routeTo}>{capatilize(name)}</Item>
                        ) : (
                            <Item key={routeTo}>
                                <Link to={`${routeTo}`}>{capatilize(name)}</Link>
                            </Item>
                        );
                    })}
                </Breadcrumb>
            </div>
        );
    };

    return <>{breadCrumbView()}</>;
};

export default BreadCrumb;
