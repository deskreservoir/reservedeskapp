import React from "react";
import "antd/dist/antd.css";
import "../../App.css";
import {Layout} from "antd";
import Title from "antd/lib/typography/Title";
import { Route, Switch} from "react-router-dom";
import SideBar from "./sider";
import home from "../../pages/HomePage/Home";
import User from "../../pages/UserPage/User";
import BreadCrumb from "../Breadcrumb/BreadCrumb";


import classes from "./LayoutDashboard.module.css";

const {Header, Footer, Content} = Layout;

const LayoutDashboard = () => {

    return (
            <div className={classes.dashboard}>
                <Layout>
                    <Header className={classes.header}>
                        <Title className={classes.title} level={3}>
                            <span >DESK RESERVOIR </span>
                        </Title>
                    </Header>
                    <Layout>
                        <SideBar/>
                        <Layout>
                            <Content className={classes.content}>
                                <BreadCrumb/>
                                <div className={classes.siteLayoutContent}>
                                    <Switch>
                                        <Route exact path="/" component={home}/>
                                        <Route exact path="/users" component={User}/>
                                        <Route path="/update/:id" />
                                    </Switch>
                                </div>
                            </Content>
                            <Footer className={classes.footer}>
                               <span > Desk Reservoir ©2021 </span>
                            </Footer>
                        </Layout>
                    </Layout>
                </Layout>
            </div>
    );
};

export default LayoutDashboard;
