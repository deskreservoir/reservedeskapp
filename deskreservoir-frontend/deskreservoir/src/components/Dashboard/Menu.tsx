import React,{FC} from 'react';
import {Menu} from 'antd';
 
export interface MenuItemsProps{
    selectKeys : [];
    
}

const MenuItems : FC<MenuItemsProps> = ({selectKeys})=>{
    return <Menu defaultSelectedKeys={selectKeys} >

    </Menu>
}

export default MenuItems;