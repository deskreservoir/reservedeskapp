import React from "react";
import { NavLink} from "react-router-dom";
import {Layout, Menu} from 'antd'
import SubMenu from "antd/lib/menu/SubMenu";
import {UserOutlined, DesktopOutlined} from "@ant-design/icons";
import {DashboardOutlined} from "@ant-design/icons";

import classes from './sidebar.module.css'

const {Sider} = Layout;
const {Item } = Menu;
const SideBar = () => {
     
    return (
        <div>
            <Sider className={classes.sidebar}>
                <div>
                <div className={classes.sideheader}>
                <img  src="https://uniim1.shutterfly.com/ng/services/mediarender/THISLIFE/021036514417/media/23148907008/medium/1501685726/enhance" alt="profile"/>
               <div className={classes.profile}>
               <h3 className={classes.ptitle}>
              
               <span>JOHN</span>
               </h3>
      <p className="text">ADMIN</p>
    </div>
  </div>

  <hr style={{height:5 }}/>
  {/* <div className="search">
    <input type="text" className="form-control w-100 border-0 bg-transparent" placeholder="Search here"/>
    <i className="fa fa-search position-absolute d-block fs-6"></i>
  </div> */}
                </div>

                <Menu className={classes.dashboardmenu} defaultSelectedKeys={["Dashboard"]} mode="inline" >
                    <Item key="1" icon={<DashboardOutlined /> }>
                        <NavLink activeClassName={classes.active} to="/" >
                            <span className={classes.textColor} ><strong>DASHBOARD</strong></span>
                        </NavLink>
                    </Item>
                    <SubMenu key="sub1"  icon={<UserOutlined/>} title="Manage Users">

                            <Item key="2">
                                <NavLink to="/users" >
                                    <span className={classes.textColor}>All Users</span>
                                </NavLink>

                            </Item>

                    </SubMenu>
                    <SubMenu key="sub2"  icon={<DesktopOutlined/>} title="Manage Desks">

                        <Item key="3">
                            <NavLink to="/desks">
                                <span className={classes.textColor}>All Desks</span>
                            </NavLink>
                        </Item>

                    </SubMenu>
                </Menu>
            </Sider>
        </div>
    );
};

export default SideBar;
