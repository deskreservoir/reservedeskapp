import {Input as AInput} from 'antd';
import styled from "styled-components";

interface inputProps{
    name:string;
    placeholder:string;
    type:string;
    value:string;
    //onChange:(e:React.FormEvent<EventTarget>)=>void;
    //onBlur:()=>void;

}
const Input = styled(AInput)`
border-radius: 0px;
 width: 350px
`;
export const TextInput =({placeholder,name,type,value} : inputProps)=>{
    return <Input placeholder={placeholder} name={name} type={type} value={value}/>
}





