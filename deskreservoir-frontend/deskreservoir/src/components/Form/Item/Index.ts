import {Form} from 'antd';
import styled from 'styled-components'

const {Item : AItem} = Form;


/** @type {AItem} */
const Item = styled(AItem) `
 display :flex;
 margin-bottom:0;
 padding-top: 8px;
 font-size: 11px;
 line-height: 11px;
 border-radius: 20rem,
 width:350px,
 margin-left : 50px;
 color: red;
`

export default Item;