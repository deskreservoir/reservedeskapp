import { Spin } from 'antd';
import classes from './index.module.css'

const Loader =()=>{
  return <div className ={classes.spin}>
    <Spin />

    <div>Loading .....</div>
  </div>
} 
 
export default Loader;