import React, {FC, useState} from 'react'
import {Button, Form, Input, Modal} from "antd";
import Item from "../../Form/Item/Index";
import './index.css';
import ReactDOM from 'react-dom';

export interface ModalInterface {
    show: boolean;
    mode: string;
    handleOnFinish: (formData:any) => void;
    handleOnFinishFailed: () => void;
    initialValues: {};
    onCancel: () => void;
    onFieldChanging: () => void;
    handleEditFormOnFinish: (formData:any) => void;
}

const AddEditModal: FC<ModalInterface> = ({
                                              show, onCancel, mode,
                                              initialValues,
                                              handleOnFinish,
                                              onFieldChanging,
                                              handleOnFinishFailed,
                                              handleEditFormOnFinish,
                                
                                          }) => {
    const [form] = Form.useForm();
    const [state, setState] = useState({username: '',name: '', surname: '', email: '',contact: '',address: '',password: '',});

    const [isLoading,setIsLoading] = useState(false);
    const {username , name, surname, email, contact, address, password} = state;

    const onDelete = ()=>{
     setIsLoading(isLoading)
     setState(state);
    }

    const footer = [
        <div key="user-form-footer" className="steps-action">
          {mode &&  (
            <Button
              danger
              disabled={isLoading }
              className="button-spacing"
              key="delete"
              onClick={onDelete}
            >
              Delete
            </Button>
          )}

          {mode && (
            <>
              <Button disabled={isLoading } onClick={onCancel}>
                cancel
              </Button>
              <Button type="primary" onClick={form.submit}>
              {`${mode === 'edit' ? "Update User" : "Add User"}`}
              </Button>
            </>
          )}
        </div>
      ];

      const Modaloverlay =()=>{
          return   <>
          {/* <Modal
          footer={footer}
              title={`${mode === 'edit' ? "Edit User" : "Add User"}`}
              visible={show}
              onCancel={onCancel}
              maskClosable={false}
          >
              <Form
                  form={form}
                  name="users"
                  labelCol={{span: 8}}
                  wrapperCol={{span: 16}}
                  initialValues={initialValues}
                  onFinish={(formData:any)=> mode === "edit" ? handleEditFormOnFinish(formData) : handleOnFinish(formData)}
                  onFinishFailed={handleOnFinishFailed}
                  onFieldsChange={onFieldChanging}
              >
                  <Item
                      name="username"
                      rules={[{required: true, message: 'Please input your username!'}]}
                      shouldUpdate
                  >
                      <Input type="text" name="username"
                             placeholder="Username" value={username || ""}/>
                  </Item>
                  <Item
                      name="name"
                      rules={[{required: true, message: 'Please input your Name!'}]} 
                      shouldUpdate
                  >
                      <Input data-testid="fname" type="text" name="name" value={name || ""}
                             placeholder="Full Name"/>
                  </Item>
                  <Item
                      name="surname"
                      rules={[{required: true, message: 'Please input your Surname!'}]}
                      shouldUpdate
                  >
                      <Input data-testid="lname" type="text" name="surname" value={surname || ""}
                             placeholder="Last Name"/>
                  </Item>

                  <Item
                      label=""
                      name="email"
                      rules={[{type: 'email'}]}
                      shouldUpdate
                      >
                      <Input data-testid="email" name="email" value={email || ""} type="email"
                             placeholder="Email Address" />
                  </Item>
                  <Item
                      label=""
                      name="contact"
                      rules={[{required: true, message: 'Please input your phone number!'}]}
                      shouldUpdate
                  >
                      <Input data-testid="phone" name="contact" type="tel" value={contact || ""}
                              placeholder="Phone Number"/>
                  </Item>
                  <Item
                      label=""
                      name="password"
                      rules={[{required: true, message: 'Please input your password!'}]}
                      shouldUpdate
                  >
                      <Input.Password data-testid="password" name="password" value={password || ""}
                                       placeholder="Password"/>
                  </Item>

                  <Item
                      label=""
                      name="address"
                      shouldUpdate
                      >
                      <Input.TextArea data-testid="address" name="address" value={address || ""}
                                       placeholder="Address"/>
                  </Item>

              </Form>

          </Modal> */}
      </>
      }
 
    return (
      <React.Fragment>
          {/* {ReactDOM.createPortal(<Modaloverlay/>,(document.getElementById('overlay-root')as HTMLElement))} */}

          <Modal
          footer={footer}
              title={`${mode === 'edit' ? "Edit User" : "Add User"}`}
              visible={show}
              onCancel={onCancel}
              maskClosable={false}
              destroyOnClose={true}
          >
              <Form
                  form={form}
                  name="users"
                  labelCol={{span: 8}}
                  wrapperCol={{span: 16}}
                  initialValues={initialValues}
                  onFinish={(formData:any)=> mode === "edit" ? handleEditFormOnFinish(formData) : handleOnFinish(formData)}
                  onFinishFailed={handleOnFinishFailed}
                  onFieldsChange={onFieldChanging}
              >
                  <Item
                      name="username"
                      rules={[{required: true, message: 'Please input your username!'}]}
                      shouldUpdate
                  >
                      <Input type="text" name="username"
                             placeholder="Username" value={username || ""}/>
                  </Item>
                  <Item
                      name="name"
                      rules={[{required: true, message: 'Please input your Name!'}]} 
                      shouldUpdate
                  >
                      <Input data-testid="fname" type="text" name="name" value={name || ""}
                             placeholder="Full Name"/>
                  </Item>
                  <Item
                      name="surname"
                      rules={[{required: true, message: 'Please input your Surname!'}]}
                      shouldUpdate
                  >
                      <Input data-testid="lname" type="text" name="surname" value={surname || ""}
                             placeholder="Last Name"/>
                  </Item>

                  <Item
                      label=""
                      name="email"
                      rules={[{type: 'email'}]}
                      shouldUpdate
                      >
                      <Input data-testid="email" name="email" value={email || ""} type="email"
                             placeholder="Email Address" />
                  </Item>
                  <Item
                      label=""
                      name="contact"
                      rules={[{required: true, message: 'Please input your phone number!'}]}
                      shouldUpdate
                  >
                      <Input data-testid="phone" name="contact" type="tel" value={contact || ""}
                              placeholder="Phone Number"/>
                  </Item>
                  <Item
                      label=""
                      name="password"
                      rules={[{required: true, message: 'Please input your password!'}]}
                      shouldUpdate
                  >
                      <Input.Password data-testid="password" name="password" value={password || ""}
                                       placeholder="Password"/>
                  </Item>

                  <Item
                      label=""
                      name="address"
                      shouldUpdate
                      >
                      <Input.TextArea data-testid="address" name="address" value={address || ""}
                                       placeholder="Address"/>
                  </Item>
                  
                  <Item name="userId" shouldUpdate noStyle>
                         <Input type="hidden" />
                   </Item>
                  

              </Form>

          </Modal>
      </React.Fragment>
    )
}
export default AddEditModal;