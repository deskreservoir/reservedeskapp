import React, {useState, useEffect} from "react";
import {Button, Form, Modal, Table} from "antd";
import "antd/dist/antd.css";
import {DeleteOutlined, EditOutlined, PlusCircleOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {RootStore} from "../../redux/users/store";
import {addUser, deleteUser, getSingleUser, loadUsers, updateUser} from "../../redux/users/actions";
import AddEditModal from "../UI/ModalUI";

const Tables = () => {
    const [form] = Form.useForm();
    const dispatch = useDispatch();
    const {users} = useSelector((state: RootStore) => state.data);
    const {user} = useSelector((state: RootStore) => state.data)
    const [isLoading, setLoading] = useState(false);
    const [showModal,setShowModal] = useState(false)
    const[mode,setMode] = useState('add')
    const[errorInfo,setErrorInfo] = useState("");
    const emptyUser = { username: '',name: '',surname: '',email: '',contact: '',address: '',password: '',}
    const [state, setState] = useState(emptyUser);
    

    const handleFormOnFinish = (formData:any) => { 
                dispatch(addUser(formData));
                setErrorInfo(errorInfo);
                setShowModal(false); 
                dispatch(loadUsers())
                
    }

    // useEffect(() => {
    //     dispatch(getSingleUser(user.userId))
    // }, [dispatch,user]);
    
    useEffect(() => {
        form.setFieldsValue(user)
        if (user) {
            setState({...user})
            form.setFieldsValue({userId: user.userId})
        }
    }, [form, user]);


    const handleEditFormOnFinish = (formData:any) => {
        dispatch(updateUser(formData,formData.userId)); 
        setShowModal(false);
        dispatch(loadUsers())
    }
    const handleEditFormOnFinishFailed = (errorInfo: any) => {
        setErrorInfo(errorInfo);
    }

    useEffect(() => {
        dispatch(loadUsers())
        setLoading(false)
    }, [dispatch])

    const openEditModal = (id:number,user:any)=>{
       setState(user);
       setShowModal(true);
       setMode('edit')
    }
    
    const handleOnClose = () =>{
        setState(emptyUser);
        setShowModal(false);
        setMode('add');
    }

    const onDelete = (id: number): void => {
        Modal.confirm({
            title: 'Delete This User',
            content: 'Are you sure you want to delete it?',
            okText: 'Yes, Delete it',
            cancelText: 'Cancel',
            onOk: () => {
                dispatch(deleteUser(id));
                dispatch(loadUsers());
            },
            onCancel: () => {
                setShowModal(false);
            },
        })
    }

    const handleInputChange = (e: React.FormEvent<EventTarget>) => {
        let target = e.target as HTMLInputElement;
        setState({...state, [target.name]: target.value})

    };

    const columns = [
        {
            key: '2',
            title: "User Name",
            dataIndex: "username",
            sorter: (a:any, b:any) => { return a.username.localeCompare(b.username)},
        },
        {
            key: '3',
            title: "Name",
            dataIndex: "name",
        },
        {
            key: '4',
            title: "Surname",
            dataIndex: "surname",
        },
        {
            key:'5',
            title: "Email",
            dataIndex: "email",
        },
        {
            key: '6',
            title: "Contact",
            dataIndex: "contact",
        },
        {
            key: '7',
            title: "Address",
            dataIndex: "address",
        },
        {
            title: '',
            dataIndex: 'action',
            key: '9',
            render: (_: number, userRecord: any): JSX.Element => {
                return (
                    <React.Fragment>
                        <Button data-testid="edit-user-button"
                                type="primary"
                                shape="round"
                                icon={<EditOutlined/>}
                                onClick={() => {openEditModal(userRecord.userId,userRecord);}}
                        >
                        </Button>

                        {' '}
                        <Button type="dashed" shape="round" icon={<DeleteOutlined/>}
                                onClick={(): void => onDelete(userRecord.userId!)}>
                        </Button>
                    </React.Fragment>
                )
            },
        },
    ];

    return (
        <div>
            <div style={{ display:"flex",justifyContent:"space-between",marginBottom:20,}}>
                <Button
                    type="primary"
                    shape="round"
                    style={{marginBottom: 16}}
                    icon={<PlusCircleOutlined/>}
                    onClick={() => {
                        setShowModal(true);
                    }}
                >
                   <span style={{marginBottom:0}}> Add New</span>
                </Button>
            </div>
            <Table
                loading={isLoading}
                columns={columns}
                dataSource={users.map((el:any, idx:number) => ({key: idx, ...el}))}
                pagination={ {defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}}
            />
            {showModal &&(
          <AddEditModal
            show={showModal}
            onCancel={handleOnClose}
            mode ={mode}
            handleOnFinish={handleFormOnFinish}
            initialValues={state}
            onFieldChanging={()=>handleInputChange}
            handleOnFinishFailed={()=>handleEditFormOnFinishFailed}
            handleEditFormOnFinish={handleEditFormOnFinish}
          />)}

        </div>
    );
};

export default Tables;
