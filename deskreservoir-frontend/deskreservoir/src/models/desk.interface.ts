export interface DeskType{
    deskId ?: number ;
    deskNo:number;
    description : string;
    dateBooked : string;
    fromTime :string;
    toTime : string;
    deskName : string;
    availability : string;
    deskType:string;

}