import {UserType} from "./user.interface";

export interface ModalProps{
    visible: boolean;
   // onCreate: (values: UserType) => void;
    onCancel: () => void;
    title?: string;
    onOk : () => void;
}