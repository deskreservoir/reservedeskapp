export interface UserType{
    userId ?: number ;
    username :string;
    name : string;
    surname : string;
    email :string;
    contact : string;
    address : string;
    password : string;
}