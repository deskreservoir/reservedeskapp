
import './home.scss';

const Home = () => {
  return (
    <>
    <div className="contentheading">
    <div className="welcome">
      <div className="content">
        <h1 className="welcome">Welcome to Dashboard</h1>
        <p className="hello">Hello John Doe, welcome to your awesome dashboard!</p>
      </div>
    </div>
    </div>
      <div className='cardContainer inactive'>
        <div className='card'  >
          <div className='side front'>
            <div className='img img1'></div>
            <div className='info'>
              <h2>BROWN BAG</h2>
              <p>
                A stand-on with an exceptional compact stance. Great for tight
                spaces and trailering
              </p>
            </div>
          </div>
          <div className='side back'>
            <div className='info'>
              <h2>DETAILS</h2>
              <div className='reviews'></div>
              <ul>
                <li>
                  Manage backyard gates with ease with the 36" deck option
                </li>
                <li>
                  Your choice of deck sizes ranging from 36", 48", 52" and 60"
                </li>
                <li>
                The upstreaming of the booking club.
                </li>
              </ul>
              <div className='btn'>
                <h4>Learn More</h4>
                <svg
                  fill='#333'
                  height='24'
                  viewBox='0 0 24 24'
                  width='24'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path d='M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' />
                  <path d='M0-.25h24v24H0z' fill='none' />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='cardContainer inactive'>
        <div className='card'>
          <div className='side front'>
            <div className='img img2'></div>
            <div className='info'>
              <h2>SOCIAL EVENTS </h2>
              <p>
                A high-performance zero-turn with unsurpassed strength, speed
                &amp; reliability with a warranty to match.
              </p>
            </div>
          </div>
          <div className='side back'>
            <div className='info'>
              <h2>DETAILS</h2>
              
              <ul>
                <li>Your choice of VX4 decks ranging from 60 to 72 inches</li>
                <li>
                  The 37hp Vanguard BigBlock EFI makes short work out of big
                  jobs enabling speeds up to 16mph
                </li>
                <li>Massive 24" drive tires and 13" front caster tires</li>
              </ul>
              <div className='btn'>
                <h4>Learn More</h4>
                <svg
                  fill='#333'
                  height='24'
                  viewBox='0 0 24 24'
                  width='24'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path d='M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' />
                  <path d='M0-.25h24v24H0z' fill='none' />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='cardContainer inactive'>
        <div className='card'>
          <div className='side front'>
            <div className='img img3'></div>
            <div className='info'>
              <h2>TEAM SHARING</h2>
              <p>
                If you need a tough, commercial grade engine that makes you more
                productive, look to Vanguard.
              </p>
            </div>
          </div>
          <div className='side back'>
            <div className='info'>
              <h2>DETAILS</h2>
              
              <ul>
                <li>
                  V-Twin OHV Technology provides superior balance, low
                  vibration, lower emissions, better fuel economy and higher
                  HP/Weight
                </li>
                <li>
                  Advanced Debris Management keeps engine clean and cool for
                  enhanced durability and performance
                </li>
              </ul>
              <div className='btn'>
                <h4>Learn More</h4>
                <svg
                  fill='#333'
                  height='24'
                  viewBox='0 0 24 24'
                  width='24'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path d='M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' />
                  <path d='M0-.25h24v24H0z' fill='none' />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='cardContainer inactive'>
        <div className='card'>
          <div className='side front'>
            <div className='img img4'></div>
            <div className='info'>
              <h2>KUDOS</h2>
              <p>
                If you need a tough, commercial grade engine that makes you more
                productive, look to Vanguard.
              </p>
            </div>
          </div>
          <div className='side back'>
            <div className='info'>
              <h2>DETAILS</h2>
              
              <ul>
                <li>
                  V-Twin OHV Technology provides superior balance, low
                  vibration, lower emissions, better fuel economy and higher
                  HP/Weight
                </li>
                <li>
                  Advanced Debris Management keeps engine clean and cool for
                  enhanced durability and performance
                </li>
              </ul>
              <div className='btn'>
                <h4>Learn More</h4>
                <svg
                  fill='#333'
                  height='24'
                  viewBox='0 0 24 24'
                  width='24'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path d='M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' />
                  <path d='M0-.25h24v24H0z' fill='none' />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
