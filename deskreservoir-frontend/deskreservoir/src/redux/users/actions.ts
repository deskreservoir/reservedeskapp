import * as types from "./actionType";
import {Dispatch} from "redux";
import axios from "axios";

//action creator
const url = 'https://localhost:5001/api/Users/GetAllUsers'
export type UserDetails = {
    userId: number;
    username: string;
    name: string;
    surname: string;
    email: string;
    contact: string;
    address: string;
    password: string;
}
export const getUsers = (users: UserDetails) => ({
    type: types.GET_USERS,
    payload: users
});
const userDeleted = () => ({
    type: types.DELETE_USER
});
const userAdded = () => ({
    type: types.ADD_USER
});
const userUpdated = () => ({
    type: types.UPDATE_USER,
     //payload:user.userId,
});
const getUser = (user: UserDetails) => ({
    type: types.GET_SINGLE_USER,
    payload: user,
})
export const loadUsers = () => {
    return function (dispatch: Dispatch) {
        axios.get(url).then((resp) => {
            console.log("resp", resp)
            dispatch(getUsers(resp.data))
        }).catch(error => console.log(error))
    }
}

export const deleteUser = (id: number) => {
    return function (dispatch: Dispatch) {
        axios
            .delete(`https://localhost:5001/api/Users/DeleteUser?Id=${id}`)
            .then((resp) => {
                console.log("resp", resp)
                dispatch(userDeleted())
                //dispatch(loadUsers());
            })
            .catch(error => console.log(error))
    }
}
export const addUser = (user: UserDetails) => {
    return function (dispatch: Dispatch) {
        axios.post(`https://localhost:5001/api/Users/Adduser`, user).then((resp) => {
            console.log("resp", resp)
            dispatch(userAdded())
        }).catch(error => console.log(error))
    }
}
export const getSingleUser = (id: number) => {
    return function (dispatch: Dispatch) {
        axios
            .get(`https://localhost:5001/api/Users/GetUserById?userId=${id}`)
            .then((resp) => {
                console.log("resp", resp);
                dispatch(getUser(resp.data));
            })
            .catch(error => console.log(error))
    }
}
export const updateUser = (user: UserDetails, id : number) => {

    return function (dispatch: Dispatch) {
        axios
            .put(`https://localhost:5001/api/Users/${id}`, user)
            .then((resp) => {
                console.log("update resp", user);
                dispatch(userUpdated());
            })
            .catch(error => console.log(error))
    }
}
