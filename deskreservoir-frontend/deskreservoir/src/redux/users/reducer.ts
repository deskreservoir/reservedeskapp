import * as types from "./actionType";

const initialState = {
    users: [],
    user: {},
    loading: true,
    showLoading :false
}

const UserReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case types.GET_USERS:
            return {
                ...state,
                users: action.payload,
                loading: false
            }
        case types.DELETE_USER:
        case types.ADD_USER:
        case types.UPDATE_USER:
            return {
                ...state,
                loading: false,
            }
        case types.GET_SINGLE_USER:
            return {
                ...state,
                user: action.payload,
                loading: false
            }
        case types.SHOW_TOGGLE_LOADING:
            return{
                ...state,
                showLoading:action.payload
            }
        default:
            return state;
    }
};
export default UserReducer;