import {combineReducers} from "redux"
import UserReducer from "./reducer"

const RootReducer = combineReducers({
    data: UserReducer,
})
export default RootReducer