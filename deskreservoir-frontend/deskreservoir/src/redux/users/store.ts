import {createStore, applyMiddleware} from "redux";
import RootReducer from "./root-reducer"
import thunk from "redux-thunk";
import logger from "redux-logger";

const middleware = [thunk]
// @ts-ignore
middleware.push(logger);

const store = createStore(RootReducer, applyMiddleware(thunk));
export type RootStore = ReturnType<typeof RootReducer>
export default store;

